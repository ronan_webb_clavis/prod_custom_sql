#!/bin/bash

cust=$1
echo -e "\nMail Extract Result links"
echo -e "-------------------------\n"

d1="2016-05-01"
d2="2017-03-09"

today=$(date +"%Y-%m-%d")

recipients="christina.anderson@clavisinsight.com,heath.fifield@clavisinsight.com,ronan.webb@clavisinsight.com,rory.okane@clavisinsight.com"
#recipients="ronan.webb@gmail.com"
#recipients="ronan.webb@clavisinsight.com"
subject="J&J US data extract $d1 to $d2 HARVEST ONLY"

(
echo "From: no-reply@utility-preprod.clavisinsight.com"
echo "To: $recipients"
echo "Subject: $subject"
echo -e "\n"
cat /home/adhocsql/results/"results.$today.${cust}.files"
)|/usr/sbin/sendmail $recipients

echo "Script complete."
#eoscript


