#!/bin/bash

shopt -s expand_aliases
source ~/.bash_aliases

#d1=$(date  --date="7 day ago" +"%Y-%m-%d")
#d2=$(date  --date="1 day ago" +"%Y-%m-%d")
#custid=69

echo "$1 : $2 TO $3"
custid=$1
d1=$2
d2=$3

cd /home/adhocsql
pwd
echo "Running Job for ${d1} to ${d2} for custid ${custid}"
/home/adhocsql/bin/r-non-ul-cust-3-4only.sh ${d1} ${d2} $custid
echo "Finished Job for ${d1} to ${d2} for custid ${custid}"

source $HOME/bin/setdatevars.sh;
$HOME/bin/package.sh $D0 69
$HOME/dbmail2.sh 69 $D7 $D1 $D0



