#!/bin/bash

shopt -s expand_aliases
source ~/.bash_aliases

source bin/sql.sh

## !!!!!!!!!!!!!!!!!!
## rw feb 23 2017 - exclude the 5 reviews query

MAPS="maps"

DATADIR="data"
TMPDIR="tmp"
OUTDIR="output"

sqpass="ZfQx3wek"

R1="Harvest"
R2="Search"
R3="Integrity"
R4="ImageMatching"
R5="Reviews"

R6="Availability"
R7="GeoAvailability"
R8="PcPriceandPromotionText"


##rm $DATADIR/* $TMPDIR/*

#### non UL
#$1=date from
#$2=date to
#$3=cust id

echo "Getting specs"
#SQL="SELECT customers_id,db,db2,output,folder from spec WHERE type<>'unilever' and status=1"
SQL="SELECT customers_id,db,db2,output,folder from spec WHERE type<>'unilever' and customers_id=$3"
specdb="127.0.0.1/adhocprod adhocprod ahjobbies"
echo java -cp $LIB:$LIB/* JdbcGen2noquote $specdb "$SQL"
java -cp $LIB:$LIB/* JdbcGen2noquote $specdb "$SQL" | tail -n +2 > tmp/specs
echo "specs:"
cat tmp/specs
##############################################################################################################
OIFS=$IFS
IFS=","


fdate=$1
tdate=$2

while read CUSTID db db2 output folder
do

	echo "================================================================================="
#	echo "$CUSTID $db $db2 $output"
DBDATE=$(date +"%Y-%m-%d")
## THIS (from the db) should work but NOT  if DNS broken 
###db2="product-information-replica.db.clavistechnology.com/product_information"  ## rw NOV 2016
db2="product-information-replica-$DBDATE.c4wznujzgwrt.us-west-2.rds.amazonaws.com/product_information"
	echo "$output - $folder"

	D=1

	DX=$fdate
	enddate=$(date -d "$tdate" +%s)		## in epoch secs

	while [[ $(date -d "$DX" +%s) -le $enddate ]]
	do  
		echo -e "day [$DX]";

 		D1=$DX
		D2=$DX

		setsql1;
		echo "SQL1"
		echo $SQL
		java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $TMPDIR/1.${output}.${D}.0   ### 2>/dev/null
		echo "SQL2"
		setsql2; #echo $SQL
		java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $TMPDIR/2.${output}.${D}.0   2>/dev/null
		echo "SQL5"
##		setsql5; #echo $SQL
##		java -cp $LIB:$LIB/* JdbcGen2pg $db2 clavis_ro rc223svt  "$SQL" > $TMPDIR/5.${output}.${D}.0   2>/dev/null

	if [[ $CUSTID == '107' ]]; then
		echo "SQL6"
		setsql6; #echo $SQL
		java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $TMPDIR/6.${output}.${D}.0   2>/dev/null
		echo "SQL7"
		setsql7; #echo $SQL
		java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $TMPDIR/7.${output}.${D}.0   2>/dev/null
		echo "SQL8"
		setsql8; #echo $SQL
		java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $TMPDIR/8.${output}.${D}.0   2>/dev/null
	fi
	
		DX=$(date +%Y-%m-%d -d "$DX +1 day")
		D=$((D+1))

	done

	## glue  7 days into  single report

	echo -e "\nCombining day reports"
	> $TMPDIR/h.$$; for  f in  $TMPDIR/1.${output}.*; do head -1 $f >> $TMPDIR/h.$$; done
	head -1 $TMPDIR/h.$$ > $DATADIR/${output}-$fdate-to-$tdate.${R1}.csv
	> $TMPDIR/h.$$; for  f in  $TMPDIR/2.${output}.*; do head -1 $f >> $TMPDIR/h.$$; done
	head -1 $TMPDIR/h.$$ > $DATADIR/${output}-$fdate-to-$tdate.${R2}.csv
##	> $TMPDIR/h.$$; for  f in  $TMPDIR/5.${output}.*; do head -1 $f >> $TMPDIR/h.$$; done
##	head -1 $TMPDIR/h.$$ > $DATADIR/${output}-$fdate-to-$tdate.${R5}.csv

	if [[ $CUSTID == '107' ]]; then

		> $TMPDIR/h.$$; for  f in  $TMPDIR/6.${output}.*; do head -1 $f >> $TMPDIR/h.$$; done
		head -1 $TMPDIR/h.$$ > $DATADIR/${output}-$fdate-to-$tdate.${R6}.csv
		> $TMPDIR/h.$$; for  f in  $TMPDIR/7.${output}.*; do head -1 $f >> $TMPDIR/h.$$; done
		head -1 $TMPDIR/h.$$ > $DATADIR/${output}-$fdate-to-$tdate.${R7}.csv
		> $TMPDIR/h.$$; for  f in  $TMPDIR/8.${output}.*; do head -1 $f >> $TMPDIR/h.$$; done
		head -1 $TMPDIR/h.$$ > $DATADIR/${output}-$fdate-to-$tdate.${R8}.csv

	fi
	#head -1 $TMPDIR/1.${output}.1.0  > $DATADIR/${output}.1.csv

	D=1
	DX=$fdate
	enddate=$(date -d "$tdate" +%s)		## in epoch secs

	while [[ $(date -d "$DX" +%s) -le $enddate ]]
	do  
		echo -e "day [$DX]"
		tail -n +2 $TMPDIR/1.${output}.$D.0  >> $DATADIR/${output}-$fdate-to-$tdate.${R1}.csv
		tail -n +2 $TMPDIR/2.${output}.$D.0  >> $DATADIR/${output}-$fdate-to-$tdate.${R2}.csv
##		tail -n +2 $TMPDIR/5.${output}.$D.0  >> $DATADIR/${output}-$fdate-to-$tdate.${R5}.csv

	if [[ $CUSTID == '107' ]]; then

		tail -n +2 $TMPDIR/6.${output}.$D.0  >> $DATADIR/${output}-$fdate-to-$tdate.${R6}.csv
		tail -n +2 $TMPDIR/7.${output}.$D.0  >> $DATADIR/${output}-$fdate-to-$tdate.${R7}.csv
		tail -n +2 $TMPDIR/8.${output}.$D.0  >> $DATADIR/${output}-$fdate-to-$tdate.${R8}.csv
	fi
		DX=$(date +%Y-%m-%d -d "$DX +1 day")
		D=$((D+1))

	done

	## run these reports once (no dates)
	echo "SQL3"
	setsql3; #echo $SQL
	java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $DATADIR/${output}-$fdate-to-$tdate.${R3}.csv  2>/dev/null
	echo "SQL4"
	setsql4; #echo $SQL
	java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $DATADIR/${output}-$fdate-to-$tdate.${R4}.csv  2>/dev/null

##
	#norman wisdom

	##norman $DATADIR/${output}-$fdate-to-$tdate.${R1}.csv ${MAPS}/template.${R1} ${MAPS}/aliases.${R1} > $DATADIR/${output}-$fdate-to-$tdate.${R1}.formatted.csv
	##norman $DATADIR/${output}-$fdate-to-$tdate.${R2}.csv ${MAPS}/template.${R2} ${MAPS}/aliases.${R2} > $DATADIR/${output}-$fdate-to-$tdate.${R2}.formatted.csv
	##norman $DATADIR/${output}-$fdate-to-$tdate.${R3}.csv ${MAPS}/template.${R3} ${MAPS}/aliases.${R3} > $DATADIR/${output}-$fdate-to-$tdate.${R3}.formatted.csv
	##norman $DATADIR/${output}-$fdate-to-$tdate.${R4}.csv ${MAPS}/template.${R4} ${MAPS}/aliases.${R4} > $DATADIR/${output}-$fdate-to-$tdate.${R4}.formatted.csv
	##norman $DATADIR/${output}-$fdate-to-$tdate.${R5}.csv ${MAPS}/template.${R5} ${MAPS}/aliases.${R5} > $DATADIR/${output}-$fdate-to-$tdate.${R5}.formatted.csv

	## //TODO norman 6 7 8 !?
##

	distdir=${OUTDIR}/${folder}-${output}/$(date +"%Y-%m-%d")
	distdir=${OUTDIR}/${folder}-${output}/${tdate}
	mkdir -p ${distdir}
	mv $DATADIR/${output}*.csv $distdir

done < tmp/specs



IFS=$OIFS

