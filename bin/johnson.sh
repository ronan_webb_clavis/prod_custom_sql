#!/bin/bash

shopt -s expand_aliases
source ~/.bash_aliases

d1=$(date  --date="7 day ago" +"%Y-%m-%d")
d2=$(date  --date="1 day ago" +"%Y-%m-%d")
custid=87

cd /home/adhocsql
pwd
echo "Running Job for ${d1} to ${d2} for custid ${custid}"
/home/adhocsql/bin/r-non-ul-cust.sh $d1 $d2 $custid
echo "Finished Job for ${d1} to ${d2} for custid ${custid}"

