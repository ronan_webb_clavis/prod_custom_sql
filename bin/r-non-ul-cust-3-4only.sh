#!/bin/bash

shopt -s expand_aliases
source ~/.bash_aliases

source bin/sql.sh

## !!!!!!!!!!!!!!!!!!
## rw apr  20 2017 - ONLY sql #3 and #4

MAPS="maps"

DATADIR="data"
TMPDIR="tmp"
OUTDIR="output"

sqpass="ZfQx3wek"

#R1="Harvest"
#R2="Search"
R3="Integrity"
R4="ImageMatching"
#R5="Reviews"

#R6="Availability"
#R7="GeoAvailability"
#R8="PcPriceandPromotionText"


##rm $DATADIR/* $TMPDIR/*

#### non UL
#$1=date from
#$2=date to
#$3=cust id

echo "Getting specs"
#SQL="SELECT customers_id,db,db2,output,folder from spec WHERE type<>'unilever' and status=1"
SQL="SELECT customers_id,db,db2,output,folder from spec WHERE type<>'unilever' and customers_id=$3"
specdb="127.0.0.1/adhocprod adhocprod ahjobbies"
echo java -cp $LIB:$LIB/* JdbcGen2noquote $specdb "$SQL"
java -cp $LIB:$LIB/* JdbcGen2noquote $specdb "$SQL" | tail -n +2 > tmp/specs

##############################################################################################################
OIFS=$IFS
IFS=","


fdate=$1
tdate=$2

while read CUSTID db db2 output folder
do

	echo "================================================================================="
	echo "$output - $folder"

	## run these reports once (no dates)
	echo "SQL3"
	setsql3; #echo $SQL
	java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $DATADIR/${output}-$fdate-to-$tdate.${R3}.csv  2>/dev/null
	echo "SQL4"
	setsql4; #echo $SQL
	java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $DATADIR/${output}-$fdate-to-$tdate.${R4}.csv  2>/dev/null

##
	#norman wisdom

	##norman $DATADIR/${output}-$fdate-to-$tdate.${R1}.csv ${MAPS}/template.${R1} ${MAPS}/aliases.${R1} > $DATADIR/${output}-$fdate-to-$tdate.${R1}.formatted.csv
	##norman $DATADIR/${output}-$fdate-to-$tdate.${R2}.csv ${MAPS}/template.${R2} ${MAPS}/aliases.${R2} > $DATADIR/${output}-$fdate-to-$tdate.${R2}.formatted.csv
	##norman $DATADIR/${output}-$fdate-to-$tdate.${R3}.csv ${MAPS}/template.${R3} ${MAPS}/aliases.${R3} > $DATADIR/${output}-$fdate-to-$tdate.${R3}.formatted.csv
	##norman $DATADIR/${output}-$fdate-to-$tdate.${R4}.csv ${MAPS}/template.${R4} ${MAPS}/aliases.${R4} > $DATADIR/${output}-$fdate-to-$tdate.${R4}.formatted.csv
	##norman $DATADIR/${output}-$fdate-to-$tdate.${R5}.csv ${MAPS}/template.${R5} ${MAPS}/aliases.${R5} > $DATADIR/${output}-$fdate-to-$tdate.${R5}.formatted.csv

	## //TODO norman 6 7 8 !?
##

	distdir=${OUTDIR}/${folder}-${output}/$(date +"%Y-%m-%d")
	mkdir -p ${distdir}
	mv $DATADIR/${output}*.csv $distdir

done < tmp/specs



IFS=$OIFS

