#!/bin/bash

shopt -s expand_aliases
source /home/adhocsql/.bash_aliases

#d1=$(date  --date="1 day ago" +"%Y-%m-%d")
#d2=$(date  --date="1 day ago" +"%Y-%m-%d")
d1=$(date  --date="0 day ago" +"%Y-%m-%d")
d2=$(date  --date="0 day ago" +"%Y-%m-%d")

custid=107

cd /home/adhocsql
pwd
echo "Running Job for ${d1} to ${d2} for custid ${custid}"
/home/adhocsql/bin/r-non-ul-cust.sh $d1 $d2 $custid
echo "Finished Job for ${d1} to ${d2} for custid ${custid}"

