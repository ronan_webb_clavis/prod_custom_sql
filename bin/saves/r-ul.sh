#!/bin/bash

shopt -s expand_aliases
source ~/.bash_aliases

source bin/sql-ul.sh

DATADIR="data"
TMPDIR="tmp"
OUTDIR="output"

sqpass="key3ref6"

##rm $DATADIR/* $TMPDIR/*

#### UL

echo "Getting specs"
SQL="SELECT customers_id,db,db2,output,folder from spec WHERE type='unilever' and status=1"
specdb="127.0.0.1/adhocprod adhocprod ahjobbies"
echo java -cp $LIB:$LIB/* JdbcGen2noquote $specdb "$SQL"
java -cp $LIB:$LIB/* JdbcGen2noquote $specdb "$SQL" | tail -n +2 > tmp/specs

##############################################################################################################
OIFS=$IFS
IFS=","


fdate=$1
tdate=$2

while read CUSTID db db2 output folder
do

	echo "================================================================================="
#	echo "$CUSTID $db $db2 $output"
	echo "$output - $folder"

	D=1

	DX=$fdate
	enddate=$(date -d "$tdate" +%s)		## in epoch secs

	while [[ $(date -d "$DX" +%s) -le $enddate ]]
	do  
		echo -e "day [$DX]";

 		D1=$DX
		D2=$DX

		setsql1; #echo $SQL
		java -cp $LIB:$LIB/* JdbcGen2 $db squirrel_ro $sqpass "$SQL" > $TMPDIR/1.${output}.${D}.0 2>/dev/null
		setsql2; #echo $SQL
		java -cp $LIB:$LIB/* JdbcGen2 $db squirrel_ro $sqpass "$SQL" > $TMPDIR/2.${output}.${D}.0  2>/dev/null
		setsql5; #echo $SQL
		java -cp $LIB:$LIB/* JdbcGen2pg $db2 clavis_ro rc223svt  "$SQL" > $TMPDIR/5.${output}.${D}.0  2>/dev/null

		DX=$(date +%Y-%m-%d -d "$DX +1 day")
		D=$((D+1))

	done

	## glue  7 days into  single report

	echo -e "\nCombining day reports"
	> $TMPDIR/h.$$; for  f in  $TMPDIR/1.${output}.*; do head -1 $f >> $TMPDIR/h.$$; done
	head -1 $TMPDIR/h.$$ > $DATADIR/${output}-$fdate-to-$tdate.1.csv
	> $TMPDIR/h.$$; for  f in  $TMPDIR/2.${output}.*; do head -1 $f >> $TMPDIR/h.$$; done
	head -1 $TMPDIR/h.$$ > $DATADIR/${output}-$fdate-to-$tdate.2.csv
	> $TMPDIR/h.$$; for  f in  $TMPDIR/5.${output}.*; do head -1 $f >> $TMPDIR/h.$$; done
	head -1 $TMPDIR/h.$$ > $DATADIR/${output}-$fdate-to-$tdate.5.csv

	#head -1 $TMPDIR/1.${output}.1.0  > $DATADIR/${output}.1.csv

	D=1
	DX=$fdate
	enddate=$(date -d "$tdate" +%s)		## in epoch secs

	while [[ $(date -d "$DX" +%s) -le $enddate ]]
	do  
		echo -e "day [$DX]";
		tail -n +2 $TMPDIR/1.${output}.$D.0  >> $DATADIR/${output}-$fdate-to-$tdate.1.csv
		tail -n +2 $TMPDIR/2.${output}.$D.0  >> $DATADIR/${output}-$fdate-to-$tdate.2.csv
		tail -n +2 $TMPDIR/5.${output}.$D.0  >> $DATADIR/${output}-$fdate-to-$tdate.5.csv

		DX=$(date +%Y-%m-%d -d "$DX +1 day")
		D=$((D+1))

	done

	## run these reports once
	setsql3; #echo $SQL
	java -cp $LIB:$LIB/* JdbcGen2 $db squirrel_ro $sqpass "$SQL" > $DATADIR/${output}-$fdate-to-$tdate.3.csv  2>/dev/null
	setsql4; #echo $SQL
	java -cp $LIB:$LIB/* JdbcGen2 $db squirrel_ro $sqpass "$SQL" > $DATADIR/${output}-$fdate-to-$tdate.4.csv  2>/dev/null

	distdir=${OUTDIR}/${folder}-${output}/$(date +"%Y-%m-%d")
	mkdir -p ${distdir}
	mv $DATADIR/${output}*.csv $distdir

done < tmp/specs



IFS=$OIFS


