#!/bin/bash

shopt -s expand_aliases
source ~/.bash_aliases

source bin/sql.sh

DATADIR="data"
sqpass="ZfQx3wek"

##############################################################################################################
#for D in $(seq 1 8); do  echo $D;  

D=8

### UL
SQL="SELECT customers_id,db,db2,output from spec WHERE type='unilever' and status=1"
specdb="127.0.0.1/adhocprod adhocprod ahjobbies"
#echo java -cp $LIB:$LIB/* JdbcGen2noquote $db "$SQL"
java -cp $LIB:$LIB/* JdbcGen2noquote $specdb "$SQL" | tail -n +2 > tmp/specs1

OIFS=$IFS
IFS=","

while read CUSTID db db2 output
do

        echo "================================================================================="
        echo $CUSTID, $output
        setsql1; echo "$SQL"
        java -cp $LIB:$LIB/* JdbcGen2 $db squirrel_ro key3ref6 "$SQL" > $DATADIR/1.$output.csv
        setsql2; echo $SQL
        java -cp $LIB:$LIB/* JdbcGen2 $db squirrel_ro key3ref6 "$SQL" > $DATADIR/2.$output.csv
        setsql3; echo $SQL
        java -cp $LIB:$LIB/* JdbcGen2 $db squirrel_ro key3ref6 "$SQL" > $DATADIR/3.$output.csv
        setsql4; echo $SQL
        java -cp $LIB:$LIB/* JdbcGen2 $db squirrel_ro key3ref6 "$SQL" > $DATADIR/4.$output.csv

		setsql5; echo $SQL
		java -cp $LIB:$LIB/* JdbcGen2pg $db2 clavis_ro rc223svt  "$SQL" > $DATADIR/5.$output.csv

done < tmp/specs1
IFS=$OIFS

#### non UL

SQL="SELECT customers_id,db,db2,output from spec WHERE type<>'unilever' and status=1"
specdb="127.0.0.1/adhocprod adhocprod ahjobbies"
echo java -cp $LIB:$LIB/* JdbcGen2noquote $specdb "$SQL"
java -cp $LIB:$LIB/* JdbcGen2noquote $specdb "$SQL" | tail -n +2 > tmp/specs

OIFS=$IFS
IFS=","

while read CUSTID db output
do
	echo "================================================================================="
	echo $CUSTID, $output
	setsql1; echo $SQL
	java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $DATADIR/1.$output.csv
	setsql2; echo $SQL
	java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $DATADIR/2.$output.csv
	setsql3; echo $SQL
	java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $DATADIR/3.$output.csv
	setsql4; echo $SQL
	java -cp $LIB:$LIB/* JdbcGen2 $db squirrel $sqpass "$SQL" > $DATADIR/4.$output.csv

	setsql5; echo $SQL
	java -cp $LIB:$LIB/* JdbcGen2pg $db2 clavis_ro rc223svt  "$SQL" > $DATADIR/5.$output.csv

done < tmp/specs
IFS=$OIFS



