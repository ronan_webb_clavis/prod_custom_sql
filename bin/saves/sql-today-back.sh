function setsql1() {

SQL="
SELECT rpt_product_hist_id,report_date,customers_id,brand_owner,region,online_store,brand,category,cpc,availability,trusted_product_description,trusted_rpc,trusted_upc,sub_category,imports_id,msrp,max_price,min_price,offer_price as online_price,offer_price,change_on_last_period,up_down_flag,diff_from_msrp,diff_from_msrp_pct,currency,store_number,harvest_url,geo_flag,available_to_purchase,number_of_customer_reviews,customer_review_rating,normalised_ratings,h_s3_img_url,t_s3_img_url,image_match_result,result_code,imaging_flag,manufacturer,is_competitor,product_image,dimension1,dimension2,dimension3,dimension4,dimension5,dimension6,dimension7,dimension8,day_key,abs_diff_from_msrp,diff_over_msrp,name_final_result,name_score,ec_image_result,ec_text_result,ec_image_score,ec_text_score,ec_score,ratings_score,reviews_score,reviews_12_month_score,ratings_and_reviews_score,harvest_product_description,date_of_customer_review_number_20,image_match_status_id FROM rpt_product_hist 
WHERE customers_id = '$CUSTID'
AND report_date = date_sub(current_date(),interval $D day) ;
"
}

function setsql2() {

#Search
SQL="
SELECT
report_date,online_store,brand,manufacturer,category,trusted_rpc,trusted_upc,trusted_product_description,is_competitor,dimension1,dimension2,dimension3,dimension4,dimension5,dimension6,dimension7,dimension8,search_term,priority_search_term_flag,results_per_page,rank,score,score_basic,priority_score
FROM rpt_share_of_search_hist
WHERE customers_id = '$CUSTID'
AND report_date = date_sub(current_date(),interval $D day) ;
"
}

function setsql3() {

#Integrity
SQL="
#Integrity
SELECT
integrity_results_by_rule_id,rule_name,milestone,rule_error_message,report_date,cpc,tenants_id,online_store,harvest_brand,harvest_sub_brand,harvest_category,harvest_sub_category,harvest_url,harvest_rpc,product_description,trusted_brand,trusted_sub_brand,trusted_category,trusted_sub_category,trusted_upc,trusted_gtin,upc,asin,hiddenUpc,skuNumber,retailerProductCode,imports_id,dimension1,dimension2,dimension3,dimension4,dimension5,dimension6,dimension7,dimension8,agg_done,milestone_id,trusted_rpc
FROM integrity_results_by_rule
WHERE tenants_id = '$CUSTID'
AND report_date = (select max(report_date) FROM rpt_integrity_by_dimension where customers_id = '$CUSTID');
"
}

function setsql4() {
#Imaging
#Note this query will always output the most recent imaging report
SQL="
SELECT
rpt_product_hist_id,report_date,i.customers_id,brand_owner,region,online_store,brand,category,cpc,availability,trusted_product_description,trusted_rpc,trusted_upc,sub_category,imports_id,msrp,max_price,min_price,offer_price as online_price,offer_price,change_on_last_period,up_down_flag,diff_from_msrp,diff_from_msrp_pct,currency,store_number,harvest_url,geo_flag,available_to_purchase,number_of_customer_reviews,customer_review_rating,normalised_ratings,h_s3_img_url,t_s3_img_url,image_match_result,i.result_code,imaging_flag,manufacturer,is_competitor,product_image,dimension1,dimension2,dimension3,dimension4,dimension5,dimension6,dimension7,dimension8,day_key,abs_diff_from_msrp,diff_over_msrp,name_final_result,name_score,ec_image_result,ec_text_result,ec_image_score,ec_text_score,ec_score,ratings_score,reviews_score,reviews_12_month_score,ratings_and_reviews_score,harvest_product_description,date_of_customer_review_number_20,i.image_match_status_id,result_type
FROM rpt_product_hist i
LEFT OUTER JOIN dim_image_match_status s
ON i.image_match_status_id = s.image_match_status_id
WHERE i.customers_id = '$CUSTID'
AND report_date = (SELECT max(report_date) FROM rpt_image_by_dimension WHERE customers_id = '$CUSTID');
"
}


#the postgres ratings/reviews db ### note different date syntax - no DATE_SUB() in PG
function setsql5() {
SQL="
SELECT
rpt_reviews_text_id,
customers_id,
online_store,market,imports_id,report_date,
retailer_product_code, matched_keywords,
upc,brand,product_description, brand_owner,review_date, review_rating,review_title,review_text,url,manufacturer,
is_competitor,time_of_publication,sub_category,category
FROM ratings_reviews.rpt_reviews_text 
WHERE customers_id = '$CUSTID' 
AND report_date = ( current_date - interval '$D' day) ;
"
}

