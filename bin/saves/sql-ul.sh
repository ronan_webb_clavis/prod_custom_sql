function setsql1() {

SQL="
SELECT *
FROM rpt_product_hist 
WHERE customers_id = '$CUSTID'
AND report_date = '$D1' ;
"
}

function setsql2() {

#Search
SQL="
SELECT
*
FROM rpt_share_of_search_hist
WHERE customers_id = '$CUSTID'
AND report_date = '$D1'  ;
"
}

function setsql3() {

#Integrity
SQL="
SELECT
*
FROM integrity_results_by_rule
WHERE tenants_id = '$CUSTID'
AND report_date = (select max(report_date) FROM rpt_integrity_by_dimension where customers_id = '$CUSTID');
"
}

function setsql4() {
#Imaging
#Note this query will always output the most recent imaging report
SQL="
SELECT
i.*
FROM rpt_product_hist i
LEFT OUTER JOIN dim_image_match_status s
ON i.image_match_status_id = s.image_match_status_id
WHERE i.customers_id = '$CUSTID'
AND i.report_date = (SELECT max(report_date) FROM rpt_image_by_dimension WHERE customers_id = '$CUSTID');
"
}


#the postgres ratings/reviews db ### note different date syntax - no DATE_SUB() in PG
function setsql5() {
SQL="
SELECT
*
FROM ratings_reviews.rpt_reviews_text 
WHERE customers_id = '$CUSTID' 
AND report_date = '$D1' ;
"
}

