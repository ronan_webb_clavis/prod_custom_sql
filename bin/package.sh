#!/bin/bash

shopt -s expand_aliases
source /home/adhocsql/.bash_aliases

cust=$2
###name="Johnson & Johnson US"

DATADIR="data"
OUTDIR="output"

RESULTS="results/"


echo "Getting specs"
SQL="SELECT customers_id,db,db2,output,folder from spec where customers_id=$cust"
specdb="127.0.0.1/adhocprod adhocprod ahjobbies"
#echo java -cp $LIB:$LIB/* JdbcGen2noquote $specdb "$SQL"
java -cp $LIB:$LIB/* JdbcGen2noquote $specdb "$SQL" | tail -n +2 > /tmp/specs.$$ 2>/dev/null

##############################################################################################################
OIFS=$IFS
IFS=","

fdate=$1

root="utility-preprod-webhosting"

while read CUSTID db db2 output folder
do

	name=${output}

#	echo "================================================================================="
	echo "$CUSTID $db $db2 $output"
#	echo "$output - $folder"

	distdir=${OUTDIR}/${folder}-${output}/$fdate
	#       output/gg817an76v1c-KCC_US/2016-11-02 for example


	#echo $distdir
	if  [[ -d $distdir ]]; then
		echo "Can do $distdir"

		rm -f ${distdir}/*.own.*
		rm -f ${distdir}/*.100.*
		rm -f ${distdir}/*.zip


		for  f in $distdir/*.csv; do 

			[[ $f == *"formatted.csv" ]] && continue;

			col=$(fcds $f "is_competitor" | tr -d '[:space:]') 
			if [[ $col != "" ]]; then
#				b=${f%.*} 
				echo $f "=>" $f.own.csv
				gds $f "0" $col > $f.own.csv
			else
				cp  $f $f.own.csv		## for symmetry, there is no "is_competitor" column to filter on
			fi

			head -101 $f > $f.100.csv

			zip -j $f.zip $f
			zip -j $f.100.zip $f.100.csv
			zip -j $f.own.zip $f.own.csv
			echo "adding to combined zip"
##			zip -j $distdir/$output-$fdate.combined.zip $f.own.csv
			zip -j $distdir/$output-$fdate.combined.zip $f

		done

		for  f in  $distdir/*.csv ; do 
			[[ $f != *"formatted.csv" ]] && continue
			echo "adding to formatted zip"
			zip -j $distdir/$output-$fdate.formatted.zip $f
		done

		s3cmd put --recursive ${distdir}/ --exclude="*.*" --include="*.formatted.zip" s3://$root/output/$folder/$fdate/
		s3cmd put --recursive ${distdir}/ --exclude="*.*" --include="*.csv.zip" s3://$root/output/$folder/$fdate/
		s3cmd put --recursive ${distdir}/ --exclude="*.*" --include="*.csv.own.zip" s3://$root/output/$folder/$fdate/
		s3cmd put --recursive ${distdir}/ --exclude="*.*" --include="$output-$fdate.combined.zip" s3://$root/output/$folder/$fdate/
		s3cmd put --recursive ${distdir}/ --exclude="*.*" --include="*.csv.100.zip" s3://$root/output/$folder/$fdate/

		echo -e "$name files are here thru web :\n" > "${RESULTS}/results.$fdate.$cust.files"
		s3cmd ls -r s3://${root}/output/$folder | grep $fdate/ | grep -v own | grep -v .100. | grep -v combined | cut -c 69- | while read fn ; do echo -e "http://${root}.s3-website-us-west-2.amazonaws.com/output/${fn}\n"; done  >> "${RESULTS}/results.$fdate.$cust.files"
		echo -e "\n\n$name files are here thru web (own brand) :\n" >> "${RESULTS}/results.$fdate.$cust.files"
		s3cmd ls -r s3://${root}/output/$folder | grep $fdate/ | grep own | grep -v combined | cut -c 69- | while read -r fn ; do echo "http://${root}.s3-website-us-west-2.amazonaws.com/output/${fn}"; done >> "${RESULTS}/results.$fdate.$cust.files"
		echo -e "\nand as a single zipfile :\n\n" >> "${RESULTS}/results.$fdate.$cust.files"  >> "${RESULTS}/results.$fdate.$cust.files"
		s3cmd ls -r s3://${root}/output/$folder | grep $fdate/ | grep combined | cut -c 69- | while read fn ; do echo -e "http://${root}.s3-website-us-west-2.amazonaws.com/output/${fn}\n"; done  >> "${RESULTS}/results.$fdate.$cust.files"
		echo -e "\n" >> "${RESULTS}/results.$fdate.$cust.files"

#		s3cmd ls -r s3://${root}/output/$folder/$cust.$fdate
		echo -e "\nFiles are here thru web :\n"
		s3cmd ls -r s3://${root}/output/$folder | grep $fdate/ | grep -v own | grep -v .100. | grep -v combined | cut -c 69- | while read -r fn ; do echo "http://${root}.s3-website-us-west-2.amazonaws.com/output/${fn}"; done 
		echo -e "\n\nFiles are here thru web (own brand) :\n"
		s3cmd ls -r s3://${root}/output/$folder | grep $fdate/ | grep own | grep -v combined | cut -c 69- | while read -r fn ; do echo "http://${root}.s3-website-us-west-2.amazonaws.com/output/${fn}"; done 
		echo -e "\n\nand as a single zipfile :\n"
		s3cmd ls -r s3://${root}/output/$folder | grep $fdate/ | grep combined | cut -c 69- | while read fn ; do echo -e "http://${root}.s3-website-us-west-2.amazonaws.com/output/${fn}\n"; done

		echo -e "\n\nvia command line :\n"
		echo "s3cmd ls -r s3://$root/output/${folder} | grep $fdate/"

	fi

done < /tmp/specs.$$
IFS=$OIFS

