#!/bin/bash

D0=$(date +"%Y-%m-%d")
D1=$(date --date="1 day ago" +"%Y-%m-%d")
D2=$(date --date="2 day ago" +"%Y-%m-%d")
D7=$(date --date="7 day ago" +"%Y-%m-%d")

echo "dates set:"
echo "D0:$D0 D1:$D1 D2:$D2 D7:$D7"
