#!/bin/bash


#args 
# custid fromdate todate "date run"

custid=$1
d1=$2
d2=$3
fordate=$4

echo -e "\nMail Extract Result links"
echo -e "-------------------------\n"

#d1=$(date  --date="7 day ago" +"%Y-%m-%d")
#d2=$(date  --date="1 day ago" +"%Y-%m-%d")

##today=$(date +"%Y-%m-%d")

LIB=$HOME/lib
RESULTS="$HOME/results"


specdb="127.0.0.1/adhocprod adhocprod ahjobbies"
SQL="SELECT concat(output,' (',customers_id,')') from spec WHERE type<>'unilever' and customers_id=${custid}"
custname=$(java -cp $LIB:$LIB/* JdbcGen2noquote $specdb "$SQL" 2> /dev/null | tail -n +2)

###recipients="christina.anderson@clavisinsight.com,heath.fifield@clavisinsight.com,ronan.webb@clavisinsight.com,rory.okane@clavisinsight.com"
recipients="heath.fifield@clavisinsight.com,ronan.webb@clavisinsight.com,rory.okane@clavisinsight.com"
###subject="J&J US data extract $d1 to $d2"
subject="${custname} data extract $d1 to $d2"

(
echo "From: no-reply@utility-preprod.clavisinsight.com"
echo "To: $recipients"
echo "Subject: $subject"
echo -e "\n"
cat "${RESULTS}/results.${fordate}.${custid}.files"
)|/usr/sbin/sendmail $recipients

echo "Script complete."
#eoscript


