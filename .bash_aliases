#rw jan 2013
alias copy='cp'
alias ...="cd ../.."
alias ..="cd .."
export LIB=$HOME/lib

#rw apr 2013
alias mds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVMove '
alias xds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVRemove '
alias hds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVHead '
alias rds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVReorder '
alias rdsp='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVReorderPiped '
alias cds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVCheck '
alias bcds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVBigColumns '
alias fds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVFit '
alias ads='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVAugment '

#rw may 27 2013
alias jds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVJoin '
alias tds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVTrimURL '
alias ids='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVInsertColumn '

#may 28 2013
alias gds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVGrep '
alias gtds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVGroupTotal '

#june 20 2013 
alias sds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVReorder '
#use rds instead!
#jan 14
alias fards='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVFindandReplace '

#may 2014
alias spcds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVSplitColumns '
#df
alias norman='java -cp $LIB/opencsv-2.3.jar:$LIB/ Norman '

#oct 01 2015
alias vds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVGrepVertical '
#may 2016
alias uds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVUnique '
alias fcds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVColumns '
alias ccds='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVCopyColumns '
alias gds2='java -cp $LIB/CSVutil.jar:$LIB/opencsv-2.3.jar CSVGrep2 '
#jul 2016
alias gsql="java -cp $LIB:$LIB/* JdbcGen2 "
