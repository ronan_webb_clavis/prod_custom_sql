#!/bin/bash

cust=$1
echo -e "\nMail Extract Result links"
echo -e "-------------------------\n"

d1=$(date  --date="7 day ago" +"%Y-%m-%d")
d2=$(date  --date="1 day ago" +"%Y-%m-%d")

today=$(date +"%Y-%m-%d")

recipients="christina.anderson@clavisinsight.com,heath.fifield@clavisinsight.com,ronan.webb@clavisinsight.com,rory.okane@clavisinsight.com,daire.mackenna@ascentialedge.com"
#recipients="heath.fifield@clavisinsight.com,ronan.webb@clavisinsight.com"
#recipients="ronan.webb@gmail.com"
#recipients="ronan.webb@clavisinsight.com"
subject="J&J US data extract $d1 to $d2"

(
echo "From: no-reply@utility-preprod.clavisinsight.com"
echo "To: $recipients"
echo "Subject: $subject"
echo -e "\n"
cat /home/adhocsql/results/"results.$today.${cust}.files"
)|/usr/sbin/sendmail $recipients

echo "Script complete."
#eoscript


