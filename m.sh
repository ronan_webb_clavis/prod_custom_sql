#!/bin/bash

today="2017-12-29"
d1="2017-12-22"
d2="2017-12-28"
cust=87

recipients="ronan.webb@clavisinsight.com"
subject="J&J US data extract $d1 to $d2"

(
echo "From: no-reply@utility-preprod.clavisinsight.com"
echo "To: $recipients"
echo "Subject: $subject"
echo -e "\n"
cat /home/adhocsql/results/"results.$today.${cust}.files"
)|/usr/sbin/sendmail $recipients

echo "Script complete."
#eoscript


