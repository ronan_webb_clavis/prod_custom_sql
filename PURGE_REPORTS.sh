#!/bin/bash

shopt -s expand_aliases
source /home/adhocsql/.bash_aliases

echo "Was ..."
/bin/df /
find /home/adhocsql/output '(' -name "*zip" -o -name "*csv" ')' -mtime +14 -exec rm {} \;
echo "Now ..."
/bin/df /

